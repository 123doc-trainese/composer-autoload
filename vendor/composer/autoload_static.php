<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit271efb0a9afd3f12a497afe9bb5f8455
{
    public static $files = array (
        '7cf61956a402a398d1f8f7a91bca8bdf' => __DIR__ . '/../..' . '/src/Helpers/helper.php',
    );

    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'Levanchien\\LevanchienMohinhoop\\' => 31,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Levanchien\\LevanchienMohinhoop\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit271efb0a9afd3f12a497afe9bb5f8455::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit271efb0a9afd3f12a497afe9bb5f8455::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit271efb0a9afd3f12a497afe9bb5f8455::$classMap;

        }, null, ClassLoader::class);
    }
}
