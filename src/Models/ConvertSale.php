<?php

namespace Levanchien\LevanchienMohinhoop\Models;

use Levanchien\LevanchienMohinhoop\Interfaces\Staff;

class ConvertSale extends Model implements Staff
{
    protected $table = 'tbl_convert_sale';
    protected $attributes = ['id', 'name', 'salary', 'KPI'];

    const KPI = 100;

    /**
     * Get salary of ConvertSale
     *
     * @return int
     */
    public function getSalary(): int
    {
        $temp = $this->KPI - self::KPI;
        if ($temp >= 0) {
            return 6000 + $temp * 15;
        }
        if ($temp > -20) {
            return 6000 + $temp * 10;
        }
        if ($temp < -20) {
            return 6000 + $temp * 15;
        }
        return 0;
    }
}
