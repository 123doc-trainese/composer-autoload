<?php

namespace Levanchien\LevanchienMohinhoop\Interfaces;

interface Staff
{
    public function getSalary(): int;
}
