<?php

if (!function_exists('config')) {
    function config(string $path)
    {
        echo __DIR__;
        $configs = require __DIR__ . '/../configs/config.php';
        $params = explode('.', $path);
        foreach ($params as $param) {
            if (isset($configs[$param])) {
                $configs = $configs[$param];
            } else {
                return null;
            }
        }
        return $configs;
    }
}
