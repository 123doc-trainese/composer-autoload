## Composer
- Composer là công cụ quản lý thư viện cho các project PHP riêng biệt.
- Các thành phần:
    - File composer.json chứa các thiết lập của composer cho dự án. 
    - Thư mục vendor chứa các thư viện được cài đặt.
    - File vendor/autoload.php giúp nạp các thư viện đã cài đặt theo chuẩn PSR-4.
- Khởi tạo composer: `Composer init`
- Kiểm tra phiên bản: `composer -v`
- Cập nhật Composer: `composer selfupdate`
- Xóa cache composer: `composer clearcache`
- Tìm các gói thư viện: `composer search tên_thư_viện`
- Tích hợp dự án: `composer require tên_thư_viện`
- Kiểm tra và cài đặt các thư viện trong file composer.json: `composer update`
- Xóa thư viện khỏi dự án: `composer remove vendor/package`
## composer.json schema
- Properties:
    - name: Tên của pakage, bao gồm vendor name và project name, cách nhau bởi dấu "/".
    - description
    - version
    - type: defaults to library
        - library
        - project
        - metapackage
        - composer-plugin
    - keywords: Optional
    - homepage: A url to the website of the project. Optional
    - readme: A relative path to the readme document. Optional
    - time: Release data of the version. Optional
    - license: The license of the package. This can be either a string or an array of strings. Optional
    - authors: This is an array of objects. Optional
        - name
        - email
        - homepage
        - role
    - support: thông tin nhận hỗ trợ về dự án. Optional
    - funding: A list of URLs to provide funding to the package authors for maintenance and development of new functionality. Optional
    - Package links: 
        - require: Gói chỉ được cài đặt khi đáp ứng đủ điều kiện.
        - require-dev: các yêu cầu để phát triển gói gốc. Có tùy chọn `--no-dev` để ngăn cài đặt các dependencies.
        - conflict: các gói xung đột với gói hiện tại, không được phép cài đặt cùng với gói hiện tại.
        - replace: 
        - provide:
        - suggest:
- Repositories: By default Composer only uses the packagist repository. By specifying repositories you can get packages from elsewhere.
- Config:
    - allow-plugin: Use this setting to allow only packages you trust to execute code. Set it to an object with package name patterns as keys. The values are true to allow and false to disallow while suppressing further warnings and prompts.
    ```
            {
                "config": {
                    "allow-plugins": {
                        "third-party/required-plugin": true,
                        "my-organization/*": true,
                        "unnecessary/plugin": false
                    }
                }
            }            
    ```

    - preferred-install: This option allows you to set the install method Composer will prefer to use.
    ```
                {
                    "config": {
                        "preferred-install": {
                            "my-organization/stable-package": "dist",
                            "my-organization/*": "source",
                            "partner-organization/*": "auto",
                            "*": "dist"
                        }
                    }
                }
    ```
    - use-parent-dir: When running Composer in a directory where there is no composer.json, if there is one present in a directory above Composer will by default ask you whether you want to use that directory's composer.json instead.
    - github-protocols: Defaults to ["https", "ssh", "git"]. A list of protocols to use when cloning from github.com, in priority order. 
    - github-oauth: A list of domain names and oauth keys. For example using {"github.com": "oauthtoken"} as the value of this option will use oauthtoken to access private repositories on github.
    - secure-http: Defaults to true. If set to true only HTTPS URLs are allowed to be downloaded via Composer. If you really absolutely need HTTP access to something then you can disable it, but using Let's Encrypt to get a free SSL certificate is generally a better alternative.

gitlab-domains#
Defaults to ["gitlab.com"]. A list of domains of GitLab servers. This is used if you use the gitlab repository type.

gitlab-oauth#
A list of domain names and oauth keys. For example using {"gitlab.com": "oauthtoken"} as the value of this option will use oauthtoken to access private repositories on gitlab. Please note: If the package is not hosted at gitlab.com the domain names must be also specified with the gitlab-domains option. Further info can also be found here

gitlab-token#
A list of domain names and private tokens. Private token can be either simple string, or array with username and token. For example using {"gitlab.com": "privatetoken"} as the value of this option will use privatetoken to access private repositories on gitlab. Using {"gitlab.com": {"username": "gitlabuser", "token": "privatetoken"}} will use both username and token for gitlab deploy token functionality (https://docs.gitlab.com/ee/user/project/deploy_tokens/) Please note: If the package is not hosted at gitlab.com the domain names must be also specified with the gitlab-domains option. The token must have api or read_api scope. Further info can also be found here

gitlab-protocol#
A protocol to force use of when creating a repository URL for the source value of the package metadata. One of git or http. (https is treated as a synonym for http.) Helpful when working with projects referencing private repositories which will later be cloned in GitLab CI jobs with a GitLab CI_JOB_TOKEN using HTTP basic auth. By default, Composer will generate a git-over-SSH URL for private repositories and HTTP(S) only for public.

   


## Autoload
- Autoload là tự động hóa việc nạp các file thư viện
- PSR-4 thống nhất cách thức nạp thư viện theo một chuẩn bố trí thư mục sao cho mọi lớp đều có thể được tham chiếu đến bằng cách:
`\<NamespaceName>(\<SubNamespaceNames>)*\<ClassName>`
    **Namespace**: tên vendor, tự đặt sao cho không xung đột tên thư viện khác
    **SubNamespace**: tương ứng với cấu trúc thư mục code.
    **ClassName**: Bắt buộc phải có và phải có tên file PHP trùng tên ClassName ở thư mục tương ứng với namespace cuối cùng.
```
{
    "autoload": {
        "psr-4": {
            "NameSpacePrefix\\": "src/"
        }
    }
}
```
- PSR-0 không cho phép sử dụng namespace prefix, vì vậy ta phải tạo đường dẫn đúng cho thư mục code. Nhược điểm so với PSR-4 là phải cập nhật file composer.json mỗi khi có class mới.
```
{
    "autoload": {
        "psr-0": {
            "Controllers\\HomeController": "src/",
            "Models\\User" : "src/"
        }
    }
}
```
- Classmap: This map is built by scanning for classes in all .php and .inc files in the given directories/files.
```
{
    "autoload": {
        "classmap": ["src/", "lib/", "Something.php"]
    }
}
```
- Files: 
```
    {
        "autoload": {
            "files": ["src/MyLibrary/functions.php"]
        }
    }
```
- Exclude files from classmaps: If you want to exclude some files or folders from the classmap you can use the exclude-from-classmap property. This might be useful to exclude test classes in your live environment, for example, as those will be skipped from the classmap even when building an optimized autoloader.
```
    {
        "autoload": {
            "exclude-from-classmap": ["/Tests/", "/test/", "/tests/"]
        }
    }
```
- autoload-dev: Classes needed to run the test suite should not be included in the main autoload rules to avoid polluting the autoloader in production and when other people use your package as a dependency. Therefore, it is a good idea to rely on a dedicated path for your unit tests and to add it within the autoload-dev section.
```
    {
        "autoload": {
            "psr-4": { "MyLibrary\\": "src/" }
        },
        "autoload-dev": {
            "psr-4": { "MyLibrary\\Tests\\": "tests/" }
        }
    }
```
