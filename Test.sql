-- Adminer 4.8.1 MySQL 8.0.30-0ubuntu0.22.04.1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `tbl_IT`;
CREATE TABLE `tbl_IT` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `salary` int NOT NULL,
  `bonus` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
INSERT INTO `tbl_IT` (`id`, `name`, `salary`, `bonus`) VALUES
(1,	'IT Name 1',	2000,	500),
(2,	'IT Name 2',	3000,	1000),
(3,	'IT Name 3',	4000,	2000),
(4,	'IT Name 4',	2000,	500);

DROP TABLE IF EXISTS `tbl_convert_sale`;
CREATE TABLE `tbl_convert_sale` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `salary` int NOT NULL,
  `KPI` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tbl_convert_sale` (`id`, `name`, `salary`, `KPI`) VALUES
(1,	'Convert Sale Name 1',	2000,	110),
(2,	'Convert Sale Name 2',	4000,	95),
(3,	'Convert Sale Name 3',	6000,	50);

-- 2022-09-27 10:24:42
